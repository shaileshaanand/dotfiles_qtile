

# the local virtual xrandr device
DEVICE="HDMI-1"

# get the modeline from
# gtf 1024 1280 60 | sed -n 's/.*Modeline "\([^" ]\+\)" \(.*\)/\1 \2/p'
MODELINE="1376x768_60.00  86.24  1376 1448 1592 1808  768 769 772 795  -HSync +Vsync"
NAME="1368x768_60.00"

# FIXME get rid of error messages
xrandr --delmode "$DEVICE" "${NAME}"
xrandr --rmmode "${NAME}"
xrandr --newmode ${MODELINE}
xrandr --addmode "$DEVICE" "${NAME}"
xrandr --output $DEVICE --mode $NAME --right-of eDP-1
