import subprocess
from libqtile.widget.backlight import Backlight, ChangeDirection
from libqtile.widget import base
from libqtile.utils import logger

import shlex


class MyBacklight(Backlight):
    def __init__(self, **config):
        Backlight.__init__(self, **config)

    def cmd_change_backlight(self, direction):
        if self.future and not self.future.done():
            return
        info = self._get_info()
        if not info:
            new = now = self.max_value
        else:
            new = now = info["brightness"]
        if direction is ChangeDirection.DOWN:  # down
            new = max(now - self.step, 0)
        elif direction is ChangeDirection.UP:  # up
            new = min(now + self.step, self.max_value)
        newp = max(new * 100 / info["max"], 10)
        if new != now:
            self.future = self.qtile.run_in_executor(self.change_backlight,
                                                     newp)
