#!/bin/bash
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
picom -b &
numlockx on
feh --bg-fill ~/wallpapers/blue-crystal-cube-3-1920×1080.jpg &
nm-applet &
blueman-applet &
# /usr/lib/xfce4/notifyd/xfce4-notifyd &
# clipmenud &
optimus-manager-qt &
redshift -P -O 10000 &
sxhkd &
dunst &
clipit &
# polybar -r mybar &
