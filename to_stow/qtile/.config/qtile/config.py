# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, extension, qtile, window
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger
from backlight import MyBacklight
from airtel import Airtel
import os
import subprocess
import xrp  # to read colors from .Xresources

mod = "mod4"
terminal = guess_terminal()

# An example key configuration is:
# Key([mod], "j", lazy.layout.down()),
# Key([mod], "k", lazy.layout.up()),
# Key([mod], "h", lazy.layout.left()),
# Key([mod], "l", lazy.layout.right()),
# Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
# Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
# Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
# Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
# Key([mod, "mod1"], "j", lazy.layout.flip_down()),
# Key([mod, "mod1"], "k", lazy.layout.flip_up()),
# Key([mod, "mod1"], "h", lazy.layout.flip_left()),
# Key([mod, "mod1"], "l", lazy.layout.flip_right()),
# Key([mod, "control"], "j", lazy.layout.grow_down()),
# Key([mod, "control"], "k", lazy.layout.grow_up()),
# Key([mod, "control"], "h", lazy.layout.grow_left()),
# Key([mod, "control"], "l", lazy.layout.grow_right()),
# Key([mod, "shift"], "n", lazy.layout.normalize()),
# Key([mod], "Return", lazy.layout.toggle_split()),


keys = [
    # Switch between windows in current stack pane
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus up in stack pane"),
    Key([mod], "k", lazy.layout.up(),
        desc="Move focus down in stack pane"),
    Key([mod], "h", lazy.layout.left(),
        desc="Move focus left in stack pane"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus right in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "shift"], "j", lazy.layout.flip_down(),
        desc="Move window up in current stack "),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window down in current stack "),
    Key([mod, "shift"], "h", lazy.layout.flip_left(),
        desc="Move window left in current stack "),
    Key([mod, "shift"], "l", lazy.layout.flip_right(),
        desc="Move window right in current stack "),

    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Move window up in current stack "),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Move window down in current stack "),
    # Key([mod, "control"], "h", lazy.layout.flip_left(),
    #     desc="Move window left in current stack "),
    # Key([mod, "control"], "l", lazy.layout.flip_right(),
    #     desc="Move window right in current stack "),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    Key([mod, "shift"], "q", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "shift"], "e", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

# ==========================COLORS=========================
Xresources = xrp.parse_file(os.path.expanduser(
    "~/.Xresources"), encoding="utf-8").resources
colors = {
    'background': '#212121',
    'foreground': Xresources["custom.gray50"],
    # 'primary': '#d32f2f',
    'primary_alt': Xresources["custom.color700"],
    'urgent': Xresources["custom.colora400"],
    'focused': Xresources["custom.color900"],
}
# =========================LAYOUTS========================

common_layout_props = {
    # 'fair': False,
    'lower_right': False,
    'border_width': 3,
    'border_focus': colors['primary_alt'],
    'border_normal': "#212121",
}
layouts = [
    layout.Bsp(
        **common_layout_props,
    ),
    layout.Bsp(
        name='bspg',
        margin=30,
        **common_layout_props,
    ),
    # layout.Tile(),
    # layout.MonadTall(),
    # layout.Max(),
    # layout.Stack(num_stacks=4),
    # Try more layouts by unleashing below layouts.
    # layout.Columns(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]
# ===============
# =============BAR==================================


def get_widgets():
    spacer = widget.Spacer(length=20)
    spacerx3 = widget.Spacer(length=60)
    spacer_stretch = widget.Spacer()
    color_data = {
        'background': colors['primary_alt'],
    }
    widgetlist = [
        widget.CurrentLayout(
            **color_data,
            foreground=colors['foreground'],
            fmt=' {} ',
        ),
        spacer,
        Airtel(
            **color_data
        ),
        spacer,
        widget.GroupBox(
            # padding_x=7,
            # padding_y=7,
            highlight_method='block',
            hide_unused=True,
            rounded=False,
            margin=4,
            this_current_screen_border=colors['focused'],
            background=colors['primary_alt'],
            foreground=colors['foreground'],
            inactive=colors['foreground'],
            urgent_border=colors['urgent'],
        ),
        spacerx3,
        spacerx3,
        widget.Net(format=" {down} |  {up}"),
        spacer_stretch,
        widget.CPU(
            fmt=' {}',
            **color_data,
            format='{freq_current} GHz  {load_percent}%'
        ),
        spacer,
        MyBacklight(
            backlight_name='intel_backlight',
            max_brightness_file='max_brightness',
            format='{percent:2.0%}',
            fmt=' {}',
            step=10,
            change_command='xbacklight -steps 5 -set {0}',
            logger=logger,
            ** color_data,
        ),
        spacer,
        widget.Volume(
            **color_data,
            fmt='墳 {}',
        ),
        spacer,
        widget.Memory(
            **color_data,
            fmt=' {}',
            format='{MemUsed} MB'
        ),
        spacer,
        widget.CheckUpdates(
            **color_data,
            fmt=' {}',
        ),
        spacer,
        widget.Battery(
            **color_data,
            format='{char} {percent:2.0%}',
            fmt=' {}',
            charge_char='',
            discharge_char='',
            update_interval=5,
        ),
        spacer,
        widget.Clock(
            **color_data,
            format='%I:%M %p',
            fmt=' {}'
        ),
        spacer,
        widget.Systray(
            icon_size=23,
            padding=5,
        ),
        spacer,
    ]
    return widgetlist


widget_defaults = dict(
    font='Hack Nerd Bold',
    fontsize=16,
    padding=10,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            get_widgets(),
            25,
            background=colors['background'],
            # opacity=0.5,
        ),
    ),
]
# =========================================================
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'TeamViewer'},  # Teamviewer
    {'wmclass': 'gnome-screenshot'},  # Gnome-Screenshot
    {'wmclass': 'gcr-prompter'},  # Gnome-Keyring
    {'wmclass': 'blueman-manager'},  # Blueman-Manager
    {'wmclass': 'join desktop'},  # Join
    {'wmclass': 'zoom'},  # zoom
], **common_layout_props)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# ===========CUSTOM============


@hook.subscribe.startup_once
def startup():
    script_path = os.path.expanduser('~/.config/qtile/startup.sh')
    subprocess.call([script_path])
