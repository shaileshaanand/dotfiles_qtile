import pathlib
import json
import requests


def do():
    with open(pathlib.Path("~/airteltokens.json").expanduser(), "r") as tokens_file:
        tokens = json.loads(tokens_file.read())
    headers = {
        'x-bsy-did': tokens["DID"],
        'x-bsy-utkn': tokens["UTKN"],
        'x-bsy-dt': tokens["DT"],
    }
    cookies = {}
    params = (
        ('density', 'AA'),
        ('Accept-Language', 'en'),
        ('Device-Language', 'en'),
    )

    response = requests.get('https://myairtelapp.bsbportal.com/myairtelapp/v1/home/accountsAndReminders',
                            headers=headers, params=params, cookies=cookies)

    # print(response.text)
    respdata = json.loads(response.text)[
        "data"]["accountsInfo"]["accounts"][0]
    daily = respdata["comboPlans"][0]["offer"]["data"]["availedPerDayStr"]
    addon = f'{respdata["dataBalanceInfo"]["total"]} {respdata["dataBalanceInfo"]["unit"]}'
    return f'{daily} / {addon}'


print(do())
